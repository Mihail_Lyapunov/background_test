import {Injectable} from '@angular/core';
import {User} from './user';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class FakeDataService {
 	constructor(private http: HttpClient) {	
 	}

	getFieldsData() {
       return this.http.get('fakeFieldsData.json');
	}

	getUserById(id: string): User {
		return JSON.parse(localStorage.getItem(id));
	}

	saveUser(testForm: User, id: string){
		let serialTestForm = JSON.stringify(testForm);
		localStorage.setItem(id, serialTestForm);
	}
}