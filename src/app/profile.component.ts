import {Component, OnInit, ViewChild, HostListener} from '@angular/core';
import {FakeDataService} from './fakeData.service';
import {Fields} from './fields';
import {User} from './user';
import {Router, ActivatedRoute} from '@angular/router';
import {FormGroup, FormControl, Validators, FormArray, 
	FormBuilder} from '@angular/forms';
import {SelectComponent} from './fieldsComponents/select.component';

@Component({
	moduleId: module.id,
    selector: 'profile',
    templateUrl: `./profile.component.html`,
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

	profileForm: FormGroup;
	fields: Fields;
	testForm: User = {
		name: '', 
		age: 1, 
		maritalStatus: 'Не женат / не замужем',
		university: [],
		placeOfBirth: 'Не важно',
		skills: []
	};
	id: string;
	newUser:boolean = true;
	loading: boolean = true;

	@ViewChild("select1")
	select1: SelectComponent;
	@ViewChild("select2")
	select2: SelectComponent;

	@HostListener('document:click', ['$event'])
	onDocumentClick(event) {
	    if (event.target.className.indexOf('field__select') == -1) {
	    	this.select1.showOptions = false;
	    	this.select2.showOptions = false;
	    }     
	}

	constructor(private fakeDataService: FakeDataService,
		private router: Router, private activateRoute: ActivatedRoute,
		private formBuilder: FormBuilder) {
	}

	ngOnInit() {
		this.getFieldsData().subscribe((data: Fields) => {
			this.fields = data;
			this.id = this.activateRoute.snapshot.params['id'];
			if (this.id !== 'new') this.getUserById();
			this.formBuild();
			this.loading = false;
		}, error => {
			console.log("error: ", error);
		});	
	}

	getFieldsData() {
		return this.fakeDataService.getFieldsData();
	}

	getUserById() {
		let user = this.fakeDataService.getUserById(this.id);
		if (!user) {
			this.router.navigate(['/profile', 'new']);
		} else {
			this.testForm = user;
			this.newUser = false;
		}
	}

	private formBuild() {
		let controls = {},
			formArray = []

		for (let key in this.testForm) {
			if (key == 'university') {
				this.testForm[key].forEach(value => {
					formArray.push(new FormControl(value));
				})
				if (!formArray.length) {
					formArray.push(new FormControl(''));
				}
				controls[key] = new FormArray(formArray);
			} else if (this.fields[key].required) {
				controls[key] = new FormControl(this.testForm[key], [Validators.required]);
			} else {
				controls[key] = new FormControl(this.testForm[key]);
			}
		}
		this.profileForm = new FormGroup(controls);
		this.testForm = this.profileForm.value;
	}

	addField(name:string) {
		(<FormArray>this.profileForm.controls[name]).push(new FormControl(''));
		this.testForm = this.profileForm.value;
	}

	removeField(name:string) {
		let formArray = <FormArray>this.profileForm.controls[name];
		(<FormArray>this.profileForm.controls[name]).removeAt(formArray.length-1);
		this.testForm = this.profileForm.value;
	}

	submit() {
		this.universityDataCorrect();
		this.saveUser();
	}

	saveUser() {
		let message: string;
		if (this.newUser) this.id = Math.floor(Math.random() * 99999) + '';
		this.fakeDataService.saveUser(this.testForm, this.id);
		message = this.newUser ? 'Пользователь сохранен' : 'Пользователь пересохранен';
		this.newUser = false;
		if (confirm(message + ", id пользователя: " + this.id
			+ ", перейти на страницу с данным id?")) {
			this.router.navigate(['/profile', this.id]);
			window.location.reload();
		}
	}

	private universityDataCorrect() {
		let universityArray: string[];
		this.testForm = Object.assign({}, <User>this.profileForm.value);
		this.testForm.university = this.testForm.university.slice();
		universityArray = this.testForm.university;
		for (let i = 0; i < universityArray.length; i++) {
			if (universityArray[i] == '') {
				universityArray.splice(i--, 1);
			}
		}
	}
}