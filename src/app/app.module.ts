import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {ProfileComponent} from './profile.component';
import {InputComponent} from './fieldsComponents/input.component';
import {SelectComponent} from './fieldsComponents/select.component';
import {SelectOptionComponent} from './fieldsComponents/select.option.component';
import {NumberComponent} from './fieldsComponents/number.component';
import {CheckboxComponent} from './fieldsComponents/checkbox.component';
import {CheckboxItemComponent} from './fieldsComponents/checkbox.item.component';
import {FakeDataService} from './fakeData.service';

const routes: Routes = [
	{ path: 'profile/:id', component: ProfileComponent },
	{
		path: '**',
	  	redirectTo: '/profile/new',
	  	pathMatch: 'full'
	}
];

@NgModule({
	imports: [BrowserModule, FormsModule, HttpClientModule,
		ReactiveFormsModule, RouterModule.forRoot(routes)],
	declarations: [AppComponent, ProfileComponent,
		InputComponent, SelectComponent, SelectOptionComponent,
		NumberComponent, CheckboxComponent, CheckboxItemComponent],
	providers: [FakeDataService],
	bootstrap: [AppComponent]
})
export class AppModule{}