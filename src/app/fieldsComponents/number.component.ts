import {Input, Component, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
	moduleId: module.id,
    selector: 'app-number',
    templateUrl: './number.component.html',
    styleUrls: ['./number.component.css'],
    providers: [
    	{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => NumberComponent),
			multi: true
	    }
	 ]
})
export class NumberComponent {
	value: string;

	constructor() { }

	onChange: any = () => { };
	onTouched: any = () => { };

	registerOnChange(fn) {
		this.onChange = fn;
	}

	writeValue(val) {
		val = val + ''
		if (val) {
		  this.value = val + '';
		}
	}

	registerOnTouched(fn) {
		this.onTouched = fn;
	}

	validateValue(event) {
    	let regex: RegExp = new RegExp(/^\d+$/),
    		specialKeys: Array<string>,
    		current: string,
    		next: string

   		specialKeys = [ 'Backspace', 'Tab', 'End', 'Home' ];
        if (specialKeys.indexOf(event.key) !== -1) {
            return;
        }
        current = this.value;

        next = current + event.key;
        if (next && !String(next).match(regex)) {

            event.preventDefault();
        }	
	}

	changeValue() {
		if (this.value == '') {
			this.onChange(this.value);
			this.onTouched();
			return;
		}
		this.onChange(+this.value);
		this.onTouched();
	}

	incValue() {
		let value: number = +this.value;	
		this.onChange(++value);
		this.onTouched();
		this.value = value + "";
	}

	decValue() {
		if (this.value == '') return;

		let value: number = +this.value;
		if (value == 0) {
			this.onChange(value);
			this.onTouched();
			return;
		}
		this.onChange(--value);
		this.onTouched();
		
		this.value = value + "";
	}
}