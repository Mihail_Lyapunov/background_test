import {Input, Component, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
	moduleId: module.id,
    selector: 'app-checkbox',
    templateUrl: './checkbox.component.html',
    styleUrls: ['./checkbox.component.css'],
    providers: [
    	{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => CheckboxComponent),
			multi: true
	    }
	]
})
export class CheckboxComponent {
	@Input()
	choices: string[];

	options = [];
	value: string[];

	constructor() { }

	onChange: any = () => { };
	onTouched: any = () => { };

	registerOnChange(fn) {
		this.onChange = fn;
	}

	writeValue(valueMas) {
		let val: boolean;
		
		if (valueMas) {
		  this.value = valueMas;
		}

		for (let i = 0; i < this.choices.length; i++) {
			val = this.value.indexOf(this.choices[i]) != -1 ? true: false; 
			this.options[i] = {name: this.choices[i], value: val};
		}

		this.value.splice(this.value.indexOf('Выделить все'));
	}

	registerOnTouched(fn) {
		this.onTouched = fn;
	}

	toggleCheckbox(option) {
		let ind: number = this.value.indexOf(option.name);
		option.value = !option.value;
		
		if (option.name == 'Выделить все') {
			if (option.value) {
				for (let i = 0; i < this.options.length; i++) {
					if (this.options[i].name != 'Выделить все') {
						this.value[i] = this.options[i].name;
						this.options[i].value = true;
					}
				}
			} else {
				this.options.forEach(o => o.value = false);
				this.value.splice(0, this.value.length);
			}
		} else {
			if (option.value) {
				this.value.push(option.name);
			} else {
				this.value.splice(ind, 1);
			} 
		}
		this.onChange(this.value);
		this.onTouched();
	}
}