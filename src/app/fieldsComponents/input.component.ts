import {Input, Output, EventEmitter, Component, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
	moduleId: module.id,
    selector: 'app-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.css'],
    providers: [
    	{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => InputComponent),
			multi: true
	    }
	]
})
export class InputComponent {
	@Input()
	name: string;
	
	@Input()
	wide: boolean;

	@Input()
	description: string;

	@Input()
	placeholder: string;

	@Input()
	lastChild: boolean;

	@Input()
	isFirstChild: boolean;

	@Output()
	onAdd = new EventEmitter<string>();

	@Output()
	onRemove = new EventEmitter<string>();

	_value: string;

	constructor() { }

	onChange: any = () => { };
	onTouched: any = () => { };

	get value() {
		return this._value;
	}

	set value(val) {
		this._value = val;
		this.onChange(val);
		this.onTouched();
	}

	registerOnChange(fn) {
		this.onChange = fn;
	}

	writeValue(val) {
		if (val) {
		  this.value = val;
		}
	}

	registerOnTouched(fn) {
		this.onTouched = fn;
	}

	addInput() {
		this.onAdd.emit(this.name);
	}

	removeInput() {
		this.onRemove.emit(this.name);
	}
}