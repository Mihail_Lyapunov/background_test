import {Input, Component} from '@angular/core';
  
@Component({
    selector: 'app-select-option',
    template: `<ng-content></ng-content>`
})
export class SelectOptionComponent  {
	@Input()
	value: string;
	
	constructor() {}
}