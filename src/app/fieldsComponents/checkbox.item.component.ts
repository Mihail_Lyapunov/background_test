import {Input, Component} from '@angular/core';
  
@Component({
    selector: 'app-checkbox-item',
    template: `<ng-content></ng-content>`
})
export class CheckboxItemComponent {
	constructor() {}
}