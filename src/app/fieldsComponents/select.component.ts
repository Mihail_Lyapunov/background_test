import {Input, Component, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
	moduleId: module.id,
    selector: 'app-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.css'],
    providers: [
    	{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => SelectComponent),
			multi: true
	    }
	]
})
export class SelectComponent {
	@Input()
	choices: boolean;

	@Input()
	highlightOption: boolean;

	_value: string;
	showOptions: boolean = false;

	constructor() {
	}

	onChange: any = () => { };
	onTouched: any = () => { };

	get value() {
		return this._value;
	}

	set value(val) {
		this._value = val;
		this.onChange(val);
		this.onTouched();
	}

	registerOnChange(fn) {
		this.onChange = fn;
	}

	writeValue(val) {
		if (val) {
		  this.value = val;
		}
	}

	registerOnTouched(fn) {
		this.onTouched = fn;
	}

	changeOption(option) {
		this.value = option;
		this.showOptions = false;
	}
}