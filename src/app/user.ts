export class User {
	constructor(public name:string, public age:number,
		public maritalStatus:string, public university:Array<string>,
		public placeOfBirth:string, public skills:Array<string>) {
	}
}
