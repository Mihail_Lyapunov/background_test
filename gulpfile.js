/*
    Gulpfile
    M.Lyapunov
    Примечание:
        NodeJS -v: 8.11.2
            -Быстрый способ сменить версию: 'sudo npm i -g n; sudo n 8.11.2'
        Если при запуске задач выкидывает ошибку 'Error: watch src/scripts/libs ENOSPC'
            -не пугайтесь, это баг NodeJS
        Фикс: 'echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p'
        Для запуска production: 'gulp --production'
*/

'use strict';

global.$ = {
    package: require('./package.json'),
    config: require('./gulp/config'),
    tscConfig: require('./tsconfig.json'),
    path: {
        task: require('./gulp/paths/tasks.js'),
        js: require('./gulp/paths/app.js')
    },
    gulp: require('gulp'),
    del: require('del'),
    browserSync: require('browser-sync').create(),
    gp: require('gulp-load-plugins')()
};

$.path.task.forEach(function(taskPath) {
    require(taskPath)();
});

$.gulp.task('build', $.gulp.series(
    'clean',
    $.gulp.parallel(
        'componentsStyles',
        'globalStyles',
        'tsCompile',
        'js.vendor',
        'js.ngLibraries',
        'copy.assets',
        'copy.config'
    )
));

$.gulp.task('default', $.gulp.series(
    'build',
    $.gulp.parallel(
        'watch',
        'serve'
    )
));
