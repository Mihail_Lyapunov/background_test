System.config({
  packages: {        
    '/': {
      defaultExtension: 'js',
    },
    rxjs: {
      defaultExtension: false
    }
  },
  bundles: {
    "ngLibraries/Rx.system.min.js": [
      "rxjs",
      "rxjs/*",
      "rxjs/operator/*",
      "rxjs/observable/*",
      "rxjs/scheduler/*",
      "rxjs/symbol/*",
      "rxjs/add/operator/*",
      "rxjs/add/observable/*",
      "rxjs/util/*"
    ]
  },
  paths: {
    "@angular/common": "ngLibraries/common.umd",
    "@angular/core": "ngLibraries/core.umd",
    "@angular/common/http": "ngLibraries/common-http.umd",
    "@angular/compiler": "ngLibraries/compiler.umd",
    "@angular/platform-browser-dynamic": "ngLibraries/platform-browser-dynamic.umd",
    "@angular/platform-browser": "ngLibraries/platform-browser.umd",
    "@angular/router": "ngLibraries/router.umd",
    "@angular/forms": "ngLibraries/forms.umd",
  }
});