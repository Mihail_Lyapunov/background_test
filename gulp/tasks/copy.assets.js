'use strict';

module.exports = function() {
	$.gulp.task('copy.assets', function() {
	    return $.gulp.src([$.config.src + '**/*.html',
	    					$.config.src + '**/*.json',
	    					$.config.src + '**/*.{png,jpg,svg}'], 
	    					{since: $.gulp.lastRun('copy.assets')})
		     	.pipe($.gulp.dest($.config.path.build));
	});
};