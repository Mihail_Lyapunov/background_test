'use strict';

module.exports = function() {
  $.gulp.task('tsCompile', function() {
    return $.gulp.src([$.config.src + '**/*.ts'],
                        {since: $.gulp.lastRun('tsCompile')})
        .pipe(
            $.gp.if(
                !$.config.production, $.gp.sourcemaps.init()
            )
        )
        .pipe($.gp.typescript($.tscConfig.compilerOptions))
        .pipe(
            $.gp.if(
                !$.config.production, $.gp.sourcemaps.write('.')
            )
        )
        .pipe( $.gp.minifier({

            minify: $.config.minify,
            collapseWhitespace: true,
            conservativeCollapse: true,
            minifyJS: true,
            minifyCSS: true,
            getKeptComment: function (content, filePath) {
                var m = content.match(/\/\*![\s\S]*?\*\//img);
                return m && m.join('\n') + '\n' || '';
            }

        }))
        .pipe($.gulp.dest($.config.path.build))
  })
};
