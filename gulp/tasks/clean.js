'use strict';

module.exports = function() {
  $.gulp.task('clean', function(cb) {
      return $.del([
        $.config.path.build + '*',
        '!dist/.git'
      ], {force: true});
  });
};
