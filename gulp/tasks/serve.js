'use strict';

module.exports = function() {
  $.gulp.task('serve', function() {
    $.browserSync.init($.config.browserSync);

    $.browserSync.watch([
      $.config.path.build + '**/*.*'
    ], $.browserSync.reload);
  });
};
