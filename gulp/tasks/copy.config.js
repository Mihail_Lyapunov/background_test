'use strict';

module.exports = function() {
	$.gulp.task('copy.config', function() {
	    return $.gulp.src('systemjs-config.js')
		     .pipe($.gulp.dest($.config.path.build));
	});
};