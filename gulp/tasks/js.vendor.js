'use strict';

module.exports = function() {
  $.gulp.task('js.vendor', function() {
    return $.gulp.src($.path.js.vendor)
        .pipe(
            $.gp.if(
                !$.config.production, $.gp.sourcemaps.init()
            )
        )
        .pipe($.gp.concat('vendor.js'))
        .pipe(
            $.gp.if(
                !$.config.production, $.gp.sourcemaps.write('.')
            )
        )
        .pipe( $.gp.minifier({

            minify: $.config.minify,
            collapseWhitespace: true,
            conservativeCollapse: true,
            minifyJS: true,
            minifyCSS: true,
            getKeptComment: function (content, filePath) {
                var m = content.match(/\/\*![\s\S]*?\*\//img);
                return m && m.join('\n') + '\n' || '';
            }

        }))
        .pipe($.gulp.dest($.config.path.build))
  })
};
