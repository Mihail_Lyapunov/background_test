'use strict';

module.exports = function() {
  $.gulp.task('watch', function() {
    $.gulp.watch('./src/app/**/*.scss', $.gulp.series('componentsStyles'));
    $.gulp.watch('./src/styles/**/*.scss', $.gulp.series('globalStyles'));
    $.gulp.watch('./src/**/*.ts', $.gulp.series('tsCompile'));
    $.gulp.watch(['./src/**/*.json', './src/**/*.html'], $.gulp.series('copy.assets'));
    $.gulp.watch('systemjs-config.js', $.gulp.series('copy.config'));
  });
};
