'use strict';

module.exports = {
    vendor: [
    		'node_modules/zone.js/dist/zone.js',
            'node_modules/reflect-metadata/Reflect.js',
            'node_modules/es6-shim/es6-shim.js',
            'node_modules/systemjs/dist/system.js'
    ],
    ngLibraries: [
    		'node_modules/@angular/common/bundles/common.umd.js',
            'node_modules/@angular/core/bundles/core.umd.js',
            'node_modules/@angular/common/bundles/common-http.umd.js',
            'node_modules/@angular/compiler/bundles/compiler.umd.js',
            'node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            'node_modules/@angular/platform-browser/bundles/platform-browser.umd.js',
            'node_modules/@angular/router/bundles/router.umd.js',
            'node_modules/@angular/forms/bundles/forms.umd.js',
            'node_modules/rxjs-system-bundle/Rx.system.min.js'
    ]
};