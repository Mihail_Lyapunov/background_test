'use strict';

module.exports = [
    './gulp/tasks/componentsStyles.js',
    './gulp/tasks/globalStyles.js',
    './gulp/tasks/tsCompile.js',
    './gulp/tasks/copy.config.js',
    './gulp/tasks/copy.assets.js',
    './gulp/tasks/js.vendor.js',
    './gulp/tasks/js.ngLibraries.js',
    './gulp/tasks/watch.js',
    './gulp/tasks/serve.js',
    './gulp/tasks/clean.js'
];