let argv = require('yargs').argv;
let modRewrite  = require('connect-modrewrite');

module.exports = {
    browserSync: {
        notify: false,
        server: {
            baseDir: './build',
            logPrefix: 'MS'
        },
        middleware: [
            modRewrite([
                '!\\.\\w+$ /index.html [L]'
            ])
        ],
        open: false,
        port: 3000
    },

    production: argv.production,
    src: './src/',
    path: {
        build: './build/'
    },
    minify: argv.production,
    
    autoprefixerConfig: [
        'ie >= 10',
        'ie_mob >= 10',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.4',
        'bb >= 10'
    ]
};