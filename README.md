# background_test

Тестовое задание для background

## Angular 2 проект, собранный с помощью gulp

## Для запуска приложения:

1. Установить node и npm.
2. Установить модули командой 'npm install' из корневой директории проекта.
3. Установить пакет gulp глобально командой 'npm install -g gulp@4.0.0'.
4. Пересборка и синхронизация с браузером в dev режиме запускается командой 'gulp'.
	Для сборки в продакшн запускать команду 'gulp --production'. Готовая сборка в папке build. 
